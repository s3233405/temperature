package nl.utwente.di.TemperatureTranslation;

public class Translator {
    public int calculateTemperature(int tempInCelsius) {
        return (tempInCelsius * 9/5) + 32;
    }
}
