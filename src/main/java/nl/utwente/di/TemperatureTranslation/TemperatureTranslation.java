package nl.utwente.di.TemperatureTranslation;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

/** Example of a Servlet that gets a temperature value in Celsius
 *  and returns the translated temperature value in Fahrenheit.
 */

public class TemperatureTranslation extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Translator translator;
	
    public void init() throws ServletException {
        translator = new Translator();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String title = "Temperature";
    
    // Done with string concatenation only for the demo
    // Not expected to be done like this in the project
    out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Temperature (in Celsius): " +
                   request.getParameter("temp") + "\n" +
                "  <P>Temperature (in Fahrenheit): " +
                   translator.calculateTemperature(Integer.parseInt(request.getParameter("temp"))) +
                "</BODY></HTML>");
  }
}
