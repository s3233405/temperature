package nl.utwente.di.TemperatureTranslation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
/*** Tests the Temperature Translator */
public class TestTranslator {
    @Test
    public void testTranslation() throws Exception {
        Translator translator = new Translator();
        int temp = translator.calculateTemperature(30);
        Assertions.assertEquals(86, temp, 0.0, "30 Celsius in Fahrenheit");
    }
}
